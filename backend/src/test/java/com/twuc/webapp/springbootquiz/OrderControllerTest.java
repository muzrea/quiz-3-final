package com.twuc.webapp.springbootquiz;

import com.twuc.webapp.springbootquiz.model.OrderRepository;
import com.twuc.webapp.springbootquiz.model.Products;
import com.twuc.webapp.springbootquiz.model.ProductsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class OrderControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ProductsRepository productsRepository;

    @Test
    void should_post_order() throws Exception {
        productsRepository.save(new Products("可乐", 3, "瓶", "url"));
        mockMvc.perform(post("/api/products/1/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("1"))
                .andExpect(status().is(201));
    }
}
