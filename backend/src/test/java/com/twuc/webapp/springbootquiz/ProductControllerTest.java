package com.twuc.webapp.springbootquiz;

import com.twuc.webapp.springbootquiz.model.Products;
import com.twuc.webapp.springbootquiz.model.ProductsRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class ProductControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ProductsRepository productsRepository;

    @Test
    void should_get_all_products() throws Exception {
        Products product1 = productsRepository.save(new Products("可乐", 3, "瓶", "url"));
        Products product2 = productsRepository.save(new Products("雪碧", 3, "瓶", "url"));
        List<Products> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
        MvcResult mvcResult = mockMvc.perform(get("/api/products"))
                .andExpect(status().is(200)).andReturn();
    }
}
