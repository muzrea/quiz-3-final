CREATE TABLE products(
    id BIGINT,
    name VARCHAR(255),
    price INT,
    unit VARCHAR(100),
    url VARCHAR(255),
    PRIMARY KEY (id)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8;