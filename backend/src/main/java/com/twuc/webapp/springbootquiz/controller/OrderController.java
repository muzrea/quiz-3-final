package com.twuc.webapp.springbootquiz.controller;

import com.twuc.webapp.springbootquiz.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", exposedHeaders = {"Location"})
@RequestMapping("/api")
public class OrderController {
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ProductsRepository productsRepository;

    @PostMapping("/products/{productId}/orders")
    public ResponseEntity postOrder(@PathVariable Long productId,@RequestBody Integer count) {
        Optional<Products> product = productsRepository.findById(productId);
        Order order = new Order(count);
        order.addProduct(product.get());
        orderRepository.save(order);
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .build();
    }
}
