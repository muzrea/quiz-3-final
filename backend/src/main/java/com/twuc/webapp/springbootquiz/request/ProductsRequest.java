package com.twuc.webapp.springbootquiz.request;


import javax.validation.constraints.NotNull;

public class ProductsRequest {
    @NotNull
    private String name;
    @NotNull
    private Integer price;
    @NotNull
    private String unit;
    @NotNull
    private String url;

    public ProductsRequest() {
    }

    public ProductsRequest(@NotNull String name, @NotNull Integer price, @NotNull String unit, @NotNull String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
