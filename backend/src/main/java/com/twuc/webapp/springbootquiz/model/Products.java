package com.twuc.webapp.springbootquiz.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Products {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private Integer price;
    @Column(nullable = false)
    private String unit;
    @Column(nullable = false)
    private String url;
    @ManyToMany
    @JoinColumn
    private List<Order> order;

    public Products() {
    }

    public Products(String name, Integer price, String unit, String imgUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = imgUrl;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }

    public List<Order> getOrder() {
        return order;
    }
}
