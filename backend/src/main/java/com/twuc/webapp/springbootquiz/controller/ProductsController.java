package com.twuc.webapp.springbootquiz.controller;

import com.twuc.webapp.springbootquiz.model.Products;
import com.twuc.webapp.springbootquiz.model.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", exposedHeaders = {"Location"})
@RequestMapping("/api")
public class ProductsController {
    @Autowired
    ProductsRepository productRepository;

    @GetMapping("/products")
    public ResponseEntity<List<Products>> getProducts() {
        productRepository.deleteAll();
        productRepository.save(new Products("可乐",3,"瓶","https://images.unsplash.com/photo-1534067783941-51c9c23ecefd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80"));
        productRepository.save(new Products("雪碧",3,"瓶","https://images.unsplash.com/photo-1531804055935-76f44d7c3621?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80"));
        productRepository.save(new Products("桃子",10,"斤","https://images.unsplash.com/photo-1534142499731-a32a99935397?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"));
        List<Products> products = productRepository.findAll();
        productRepository.flush();
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(products);
    }
}
