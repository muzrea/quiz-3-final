package com.twuc.webapp.springbootquiz.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    @Column
    private Integer count;
    @ManyToMany
    private List<Products> products;
    public Order() {
    }

    public Order(int count) {
        this.count = count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Products> getProducts() {
        return products;
    }

    public void addProduct(Products products) {
        this.products.add(products);
    }
}
