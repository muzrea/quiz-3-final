import {combineReducers} from "redux";
import createProductsReducer from "../home/reducer/createProductsReducer";

const reducers = combineReducers({
  createProductsReducer: createProductsReducer
});
export default reducers;