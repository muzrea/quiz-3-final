import React, {Component} from 'react';
import './product.less';
import { IoMdAdd } from "react-icons/io";

class Product extends Component {
  constructor(props){
    super(props);
  }
  addProductToOrder(){

  }

  render() {
    const productsInfo = this.props.productsInfo;
    return (
      <div className='product-info'>
        <img src={productsInfo.url} className='product-img'/>
        <p>{productsInfo.name}</p>
        <p><span>单价：{productsInfo.price}/{productsInfo.unit}</span></p>
        <button onClick={this.addProductToOrder.bind(this)}><IoMdAdd /></button>
      </div>
    );
  }
}

export default Product;