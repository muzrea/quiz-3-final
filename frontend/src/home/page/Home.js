import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import Header from "./Header";
import {getProducts} from "../action/getProducts";
import Product from "./Product";
import './home.less'

class Home extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getProducts();
  }

  render() {
    const {products} = this.props.allProducts;
    const items = products.map((item, index) => {
      return <Product productsInfo = {item} key={`product${index}`}/>
      }
    );
    return (
      <div>
        <Header/>
        <main className='main'>
          {items}
        </main>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  allProducts: state.createProductsReducer
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getProducts}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Home);

