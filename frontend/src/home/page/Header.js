import React, {Component} from 'react';
import {connect} from 'react-redux';
import { IoIosHome } from "react-icons/io";
import { IoIosCart } from "react-icons/io";
import { IoMdAdd } from "react-icons/io";
import "./header.less"

function mapStateToProps(state) {
  return {};
}

class Header extends Component {
  render() {
    return (
      <header className='header'>
        <div className='header-item'><IoIosHome className='header-icon'/><span>商城</span></div>
        <div className='header-item'><IoIosCart className='header-icon'/><span>订单</span></div>
        <div className='header-item'><IoMdAdd className='header-icon'/><span>添加商品</span></div>
      </header>
    );
  }
}

export default connect(
  mapStateToProps,
)(Header);