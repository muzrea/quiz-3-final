export const getProducts = () => (dispatch) => {
  fetch("http://localhost:8080/api/products",{method: 'get'})
    .then(res => res.json())
    .then(result => {
        dispatch({
          type: "GET_ALL_PRODUCTS",
          products: result
        })
      }
    )
};