1. 构建商城首页
 - 实现商城静态页面
 - 完成查询全部商品的api
 - 从获取全部商品的api中获取商品信息
 - 将获取到的商品信息展示在页面

2. 实现商品首页添加商品
 - 实现添加商品的前端组件
 - 点击提交能够将商品信息添加到订单，如果回到订单页面可查看到新添加的商品。
  - 点击提交将商品信息发送给添加订单api，如果该商品在订单中已经存在，则订单中商品数目加一，否则新添加一条数目为一的商品，在数据库中成功添加订单
  - 成功添加订单后，回到订单页面可查看到新添加的商品
 - 添加请求还未返回时，添加按钮不可点击，添加成功后，可继续添加商品

3. 进入订单页面显示商品订单列表
 - 实现订单页面的静态页面
 - 完成查询全部订单的api
 - 从查询全部订单的api中获取商品信息
 - 将获取到的全部信息展示在页面

4. 无订单时，显示“暂无订单，返回商城页面继续购买”，点击“商城页面”返回到商城页

5. 在订单页面点击删除按钮，删除订单中的该商品
 - 删除成功
 - 删除失败

6. 添加商品
 - 实现添加商品静态页面
 - 点击添加商品的提交按钮，将商品信息添加到所有商品，如果回到商城页面可查看到新添加的商品。
  - 字段均不能为空，价格为数字。若不满足格式\，“提交”按钮不可点击
  - 点击提交将商品信息发送给添加商品api
  - 如果商品已经存在，点击“提交”后，弹框中提示“商品名称已存在，请输入新的商品名称”
  - 如果商品不存在则在数据库中成功添加商品
  - 成功添加订单后，回到商品页面可查看到新添加的商品信息
         